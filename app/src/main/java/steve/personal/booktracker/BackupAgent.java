package steve.personal.booktracker;

import android.app.backup.BackupAgentHelper;
import android.app.backup.FileBackupHelper;

import com.orm.util.NamingHelper;

import java.io.File;

import steve.personal.booktracker.author.Author;
import steve.personal.booktracker.book.Book;

/**
 * Created by Steve on 28-Apr-16.
 */
public class BackupAgent extends BackupAgentHelper {

    private static final String KEY = "book_data_dbs";

    private static final String[] TABLES = {
            NamingHelper.toSQLName(Author.class),
            NamingHelper.toSQLName(Book.class),
    };

    @Override
    public void onCreate() {
        FileBackupHelper dbs = new FileBackupHelper(this, TABLES);
        addHelper(KEY, dbs);
    }

    @Override
    public File getFilesDir() {
        File path = getDatabasePath(TABLES[0]);
        return path.getParentFile();
    }
}
