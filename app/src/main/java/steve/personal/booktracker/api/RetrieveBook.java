package steve.personal.booktracker.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.InvalidParameterException;

import steve.personal.booktracker.R;
import steve.personal.booktracker.book.Book;

/**
 * Created by Steve on 31-Mar-16.
 */
public class RetrieveBook extends AsyncTask<String, Void, Response> {

    private static final String URL = "https://www.googleapis.com/books/v1/volumes?q=isbn:%s&key=%s";
    private static final String REQUEST_METHOD = "GET";
    private static final int TIMEOUT = 5000;
    private static final int SUCCESS_CODE = 200;
    private final RetrieveBookCallback mCallback;
    private final Context mContext;
    private boolean mConnected = true;
    private String mIsbn;

    public <T extends Context & RetrieveBookCallback> RetrieveBook(T mCallback) {
        this.mCallback = mCallback;
        mContext = mCallback;
    }


    @Override
    protected void onPreExecute() {
        mCallback.onBegin();
        mConnected = isNetworkConnected();
    }

    protected boolean isNetworkConnected() {

        ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    protected Response doInBackground(String... isbns) {
        if (isbns.length != 1) {
            throw new InvalidParameterException(mContext.getString(R.string.one_isbn_expected));
        }
        mIsbn = isbns[0];
        if (!mConnected) {
            return Response.NO_NETWORK;
        } else if (isCancelled()) {
            return Response.CANCELLED;
        }

        String urlString = String.format(URL, mIsbn, mContext.getString(R.string.google_books_api_key));
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(REQUEST_METHOD);
            connection.setReadTimeout(TIMEOUT);
            connection.setConnectTimeout(TIMEOUT);
            int responseCode = connection.getResponseCode();
            if (responseCode != SUCCESS_CODE) {
                return Response.create(responseCode);
            }
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            JSONObject responseJson = new JSONObject(sb.toString());
            return Response.create(SUCCESS_CODE, responseJson);
        } catch (MalformedURLException e1) {
            return Response.URL_ERROR;
        } catch (SocketTimeoutException e2) {
            return Response.TIMEOUT;
        } catch (IOException e3) {
            return Response.IO_ERROR;
        } catch (JSONException e4) {
            return Response.JSON_ERROR;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    @Override
    protected void onCancelled() {
        mCallback.onEnd();
        if (mConnected) {
            mCallback.onError(mContext.getString(R.string.request_cancelled), true, null);
        } else {
            mCallback.onError(mContext.getString(R.string.no_network), true, mIsbn);
        }
        super.onCancelled();
    }

    @Override
    protected void onPostExecute(Response response) {

        mCallback.onEnd();
        if (response == Response.CANCELLED) {
            mCallback.onError(mContext.getString(R.string.request_cancelled), true, null);
        } else if (response == Response.TIMEOUT) {
            mCallback.onError(mContext.getString(R.string.request_timed_out), true, mIsbn);
        } else if (response == Response.IO_ERROR) {
            mCallback.onError(mContext.getString(R.string.io_exception), true, mIsbn);
        } else if (response == Response.URL_ERROR) {
            mCallback.onError(mContext.getString(R.string.invalid_url), false, null);
        } else if (response == Response.JSON_ERROR) {
            mCallback.onError(mContext.getString(R.string.invalid_response), false, null);
        } else if (response == Response.NO_NETWORK) {
            mCallback.onError(mContext.getString(R.string.no_network), true, mIsbn);
        } else if (response.result != 200) {
            mCallback.onError(
                    String.format(mContext.getString(R.string.wrong_code), response.result), false, mIsbn);
        } else {

            try {
                parse(response.object);
            } catch (JSONException e) {
                mCallback.onError(String.format(mContext.getString(R.string.json_error), e.getMessage()), false, null);
            }

        }

    }

    private String getAuthor(JSONArray arr) throws JSONException {
        if (arr.length() == 0) {
            return mContext.getString(R.string.blank_author);
        }
        StringBuilder sb = new StringBuilder(arr.getString(0));
        for (int i = 1; i < arr.length(); i++) {
            sb.append(", ").append(arr.getString(i));
        }
        return sb.toString();
    }

    private void parse(JSONObject json) throws JSONException {
        int numItems = json.getInt(mContext.getString(R.string.books_api_total_items));
        if (numItems == 0) {
            mCallback.onError(String.format(mContext.getString(R.string.no_books_found), mIsbn), false, null);
        } else if (numItems > 1) {
            mCallback.onError(String.format(mContext.getString(R.string.multiple_books_found), mIsbn), false, null);
        } else {
            JSONObject bookObject = json.getJSONArray(mContext.getString(R.string.books_api_items)).getJSONObject(0);
            JSONObject volumeInfo = bookObject.getJSONObject(mContext.getString(R.string.books_api_volume_info));
            String title = volumeInfo.getString(mContext.getString(R.string.books_api_title));
            JSONArray authors = volumeInfo.getJSONArray(mContext.getString(R.string.books_api_authors));
            String author = getAuthor(authors);
            Book book = new Book(title, author);
            mCallback.onSuccess(book);
        }
    }

}

class Response {

    static final Response CANCELLED = new Response(0, null);
    static final Response TIMEOUT = new Response(-1, null);
    static final Response IO_ERROR = new Response(-2, null);
    static final Response NO_NETWORK = new Response(-3, null);
    static final Response JSON_ERROR = new Response(-4, null);
    static final Response URL_ERROR = new Response(-5, null);
    final int result;
    final JSONObject object;

    private Response(int res, JSONObject object) {
        this.result = res;
        this.object = object;
    }

    public static Response create(int result) {
        return new Response(result, null);
    }

    public static Response create(int result, JSONObject object) {
        return new Response(result, object);
    }


}