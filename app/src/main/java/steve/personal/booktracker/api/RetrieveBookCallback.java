package steve.personal.booktracker.api;

import steve.personal.booktracker.book.Book;

/**
 * Created by Steve on 31-Mar-16.
 */
public interface RetrieveBookCallback {

    void onBegin();

    void onEnd();

    void onError(String message, boolean useSnackbar, String isbn);

    void onSuccess(Book book);


}
