package steve.personal.booktracker;

import android.app.ProgressDialog;
import android.app.backup.BackupManager;
import android.app.backup.RestoreObserver;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import butterknife.Bind;
import butterknife.ButterKnife;
import steve.personal.booktracker.api.RetrieveBook;
import steve.personal.booktracker.api.RetrieveBookCallback;
import steve.personal.booktracker.author.Author;
import steve.personal.booktracker.author.AuthorsFragment;
import steve.personal.booktracker.book.Book;
import steve.personal.booktracker.book.BooksFragment;

/**
 * Created by Steve on 31-Mar-16.
 */
public class MainActivity extends AppCompatActivity implements RetrieveBookCallback {


    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.tabs)
    TabLayout mTabLayout;

    @Bind(R.id.container)
    ViewPager mViewPager;

    @Bind(R.id.fab)
    FloatingActionButton mAddButton;
    @Bind(R.id.fab2)
    FloatingActionButton mBarcodeButton;

    private ProgressDialog mProgressDialog;
    private SectionsPagerAdapter mSectionsPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mTabLayout.setupWithViewPager(mViewPager);

        mAddButton.setOnClickListener(view -> {
            switch (mViewPager.getCurrentItem()) {
                case 0:
                    addBook(view);
                    break;
                case 1:
                    addAuthor(view);
                    break;
            }
        });

        mBarcodeButton.setOnClickListener(v -> doScan());


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    mBarcodeButton.hide();
                } else {
                    mBarcodeButton.show();
                }

                //cancel any action modes
                FinishActionMode f = mSectionsPagerAdapter.getFragment(position ^ 1);
                if (f != null) {
                    f.finishActionMode();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }


  /*  private void restore(final View view) {
        BackupManager bm = new BackupManager(this);
        int code = bm.requestRestore(new RestoreWatcher());
        if (code == 0) {
            Snackbar.make(view, R.string.restore_scheduled, Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(view, R.string.unable_restore, Snackbar.LENGTH_LONG)
                    .setAction(R.string.retry, v -> restore(view))
                    .show();
        }
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_share:
                shareScreen();
                return true;
           /* case R.id.action_backup: {
                BackupManager bm = new BackupManager(this);
                bm.dataChanged();
                Snackbar.make(mAddButton, R.string.backup_scheduled, Snackbar.LENGTH_SHORT).show();
            }
            return true;
            */
        /*    case R.id.action_restore: {


                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.data_restore_confirm_title)
                        .setMessage(R.string.data_restore_confirm_message)
                        .setPositiveButton(android.R.string.yes, (dialogInterface, button) -> {
                            restore(mAddButton);
                        })
                        .setNegativeButton(android.R.string.cancel, null).show();

            }
            return true;
            */
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    private void addBook(final View view) {
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.text_input_book, null);
        final EditText title = (EditText) viewInflated.findViewById(R.id.title);
        final AutoCompleteTextView author = (AutoCompleteTextView) viewInflated.findViewById(R.id.author);
        addAutoComplete(author);
        title.requestFocus();

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.add_author))
                .setView(viewInflated)
                .setPositiveButton(getString(R.string.add), (dialog, which) -> {
                    String T = title.getText().toString().trim();
                    String A = author.getText().toString().trim();
                    if (T.isEmpty() || A.isEmpty()) {
                        String message = T.isEmpty() ? getString(R.string.no_title) : getString(R.string.no_author);
                        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                                .setAction(R.string.retry, v -> addBook(view))
                                .show();
                    } else {
                        Book book = new Book(T, A);
                        book.save();
                        BooksFragment frag = mSectionsPagerAdapter.getFragment(0);
                        frag.refresh();
                        BackupManager bm = new BackupManager(this);
                        bm.dataChanged();
                        Snackbar.make(view, String.format(getString(R.string.record_added), book.getTitle()), Snackbar.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel())
                .show();
    }

    private void addAuthor(final View view) {

        View viewInflated = LayoutInflater.from(this).inflate(R.layout.text_input_author, null);
        final AutoCompleteTextView input = (AutoCompleteTextView) viewInflated.findViewById(R.id.input);
        addAutoComplete(input);
        input.requestFocus();
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.add_author))
                .setView(viewInflated)
                .setPositiveButton(getString(R.string.add), (dialog, which) -> {
                    String name = input.getText().toString().trim();
                    if (name.isEmpty()) {
                        Snackbar.make(view, getString(R.string.no_author), Snackbar.LENGTH_LONG)
                                .setAction(R.string.retry, v -> addAuthor(view))
                                .show();
                    } else {
                        Author author = new Author(name);
                        author.save();
                        AuthorsFragment frag = mSectionsPagerAdapter.getFragment(1);
                        frag.refresh();
                        BackupManager bm = new BackupManager(this);
                        bm.dataChanged();
                        Snackbar.make(view, String.format(getString(R.string.record_added), author.getName()), Snackbar.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel())
                .show();
    }


    private void addAutoComplete(AutoCompleteTextView textView) {

        SortedSet<String> names = new TreeSet<>();
        List<Author> authors = Author.listAll(Author.class);
        for (Author author : authors) {
            names.add(author.getName());
        }
        List<Book> books = Book.listAll(Book.class);
        for (Book book : books) {
            names.add(book.getAuthor());
        }

        ArrayAdapter<Object> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, names.toArray());
        textView.setAdapter(adapter);
    }

    private void doScan() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        integrator.setPrompt(getString(R.string.scan_barcode));
        integrator.setBarcodeImageEnabled(true);
        integrator.initiateScan();
    }


    private boolean isEAN13(String format) {
        final String ean = getString(R.string.ean_13);
        return format != null && format.toUpperCase().equals(ean);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult == null) {
            super.onActivityResult(requestCode, resultCode, intent);
            return;
        }
        String scanContent = scanResult.getContents();
        String scanFormat = scanResult.getFormatName();
        if (scanContent != null) {
            if (isEAN13(scanFormat)) {
                new RetrieveBook(this).execute(scanContent);
            } else {
                Snackbar.make(findViewById(android.R.id.content), R.string.invalid_barcode, Snackbar.LENGTH_LONG)
                        .setAction(R.string.retry, v -> doScan())
                        .show();
            }
        }

    }

    @Override
    public void onBegin() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle(getString(R.string.connecting_books_api));
        mProgressDialog.setMessage(getString(R.string.retrieving_info));
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();
    }

    @Override
    public void onEnd() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    public void onError(String message, boolean useSnackbar, final String isbn) {

        if (useSnackbar) {
            Snackbar sb = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
            if (isbn != null) {
                sb.setAction(R.string.retry, v -> new RetrieveBook(MainActivity.this).execute(isbn));
            }
            sb.show();

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle(message);
            if (isbn == null) {
                builder.setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());
            } else {
                builder.setPositiveButton(R.string.retry, (dialog, which) -> new RetrieveBook(MainActivity.this).execute(isbn))
                        .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());
            }
            builder.show();
        }

    }

    @Override
    public void onSuccess(Book book) {
        book.save();
        BooksFragment frag = mSectionsPagerAdapter.getFragment(0);
        frag.refresh();
        Snackbar.make(findViewById(android.R.id.content), String.format(getString(R.string.record_added), book.getTitle()), Snackbar.LENGTH_SHORT).show();
    }


    private String getShareableText() {

        switch (mViewPager.getCurrentItem()) {
            case 0: {
                BooksFragment fragment = mSectionsPagerAdapter.getFragment(0);
                return fragment.getText();
            }
            case 1: {
                AuthorsFragment fragment = mSectionsPagerAdapter.getFragment(1);
                return fragment.getText();
            }
            default:
                throw new RuntimeException("The fragment is neither one nor two?!");
        }

    }

    private void shareScreen() {
        String text = getShareableText();
        if (TextUtils.isEmpty(text)) {
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_share), Snackbar.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.setType("text/plain");
            //sendIntent.setPackage("com.whatsapp");
            startActivity(intent);
        }

    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private Map<Integer, Fragment> map = new HashMap<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public <T extends Fragment> T getFragment(int key) {
            return (T) map.get(key);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    Fragment fragment = new BooksFragment();
                    map.put(position, fragment);
                    return fragment;
                }
                case 1: {
                    Fragment fragment = new AuthorsFragment();
                    map.put(position, fragment);
                    return fragment;
                }
            }
            throw new IllegalStateException();
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            map.remove(position);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.book_tab_title);
                case 1:
                    return getString(R.string.author_tab_title);
            }
            return null;
        }
    }

    private class RestoreWatcher extends RestoreObserver {
        //nothing for now
    }

}

