package steve.personal.booktracker.book;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Steve on 31-Mar-16.
 */
public class BookAdapter extends ArrayAdapter<Book> {

    private static final int LAYOUT = android.R.layout.simple_list_item_activated_2;
    private final List<Book> mBooks;

    public BookAdapter(Context context, List<Book> objects) {
        super(context, LAYOUT, objects);
        mBooks = objects;
    }

    @Override
    public void notifyDataSetChanged() {
        mBooks.clear();
        mBooks.addAll(Book.listAll(Book.class));
        Collections.sort(mBooks);
        super.notifyDataSetChanged();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(LAYOUT, null);
        }
        TextView textView = (TextView) v.findViewById(android.R.id.text1);
        textView.setText(mBooks.get(position).getTitle());
        textView = (TextView) v.findViewById(android.R.id.text2);
        textView.setText(mBooks.get(position).getAuthor());
        return v;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Book book : mBooks) {
            sb.append('\n').append(book.getTitle()).append(" by ").append(book.getAuthor());
        }
        return sb.toString();
    }
}
