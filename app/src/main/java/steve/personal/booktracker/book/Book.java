package steve.personal.booktracker.book;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;

import steve.personal.booktracker.author.Author;

/**
 * Created by Steve on 31-Mar-16.
 */
public class Book extends SugarRecord implements Comparable<Book> {

    long timeAdded;
    String title;
    String author;

    public Book() {

    }

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
        timeAdded = System.currentTimeMillis();
    }


    public long getTimeAdded() {
        return timeAdded;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public int compareTo(@NonNull Book another) {
        int x = Author.getSurname(author).compareTo(Author.getSurname(another.author));
        if (x == 0) {
            return Long.compare(timeAdded, another.timeAdded);
        }
        return x;
    }


}
