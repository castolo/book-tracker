package steve.personal.booktracker.author;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Steve on 31-Mar-16.
 */
public class AuthorAdapter extends ArrayAdapter<Author> {

    private static final int LAYOUT = android.R.layout.simple_list_item_activated_1;
    private final List<Author> mAuthors;


    public AuthorAdapter(Context context, List<Author> objects) {
        super(context, LAYOUT, objects);
        mAuthors = objects;
    }

    @Override
    public void notifyDataSetChanged() {
        mAuthors.clear();
        mAuthors.addAll(Author.listAll(Author.class));
        Collections.sort(mAuthors);
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(LAYOUT, null);
        }
        TextView textView = (TextView) v.findViewById(android.R.id.text1);
        textView.setText(mAuthors.get(position).getName());
        return v;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Author author : mAuthors) {
            sb.append('\n').append(author.getName());
        }
        return sb.toString();

    }
}
