package steve.personal.booktracker.author;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;

/**
 * Created by Steve on 31-Mar-16.
 */
public class Author extends SugarRecord implements Comparable<Author> {

    long timeAdded;
    String name;

    public Author() {

    }

    public Author(String author) {
        this.name = author;
        timeAdded = System.currentTimeMillis();
    }

    private static String cut(String name, String delims) {

        for (char delim : delims.toCharArray()) {
            int idx = name.indexOf(delim);
            if (idx > -1) {
                name = name.substring(0, idx);
            }
        }
        return name;
    }

    public static String getSurname(String name) {

        name = cut(name, ",/+&");

        if (name.contains(" and ")) {
            name = name.substring(0, name.indexOf(" and "));
        }

        String[] arr = name.trim().split("\\s+");
        for (int i = 1; i < arr.length; i++) {
            if (Character.isLowerCase(arr[i].charAt(0))) {
                StringBuilder sb = new StringBuilder(arr[i]);
                for (int j = i + 1; j < arr.length; j++) {
                    sb.append(' ').append(arr[j]);
                }
                return sb.toString();
            }
        }
        return arr[arr.length - 1];

    }

    public long getTimeAdded() {
        return timeAdded;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(@NonNull Author another) {

        String A = getSurname(name);
        String B = getSurname(another.name);
        int comp = A.compareTo(B);
        if (comp == 0) {
            return name.compareTo(another.name);
        }
        return comp;
    }
}
