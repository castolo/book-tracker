package steve.personal.booktracker.author;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListView;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import steve.personal.booktracker.FinishActionMode;
import steve.personal.booktracker.R;

/**
 * Created by Steve on 31-Mar-16.
 */
public class AuthorsFragment extends ListFragment implements FinishActionMode {

    private AuthorAdapter mAdapter;
    private ActionMode mActionMode;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        List<Author> authors = Author.listAll(Author.class);
        Collections.sort(authors);
        mAdapter = new AuthorAdapter(getActivity(), authors);
        setListAdapter(mAdapter);
    }

    private void init() {
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        getListView().setOnItemLongClickListener((parent, view, position, id) -> {
            ((ListView) parent).setItemChecked(position, ((ListView) parent).isItemChecked(position));
            return false;
        });
        getListView().setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            private Set<Author> mSelected = new HashSet<>();

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mActionMode = mode;
                getActivity().getMenuInflater().inflate(R.menu.menu_cab, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        Author.deleteInTx(mSelected);
                        String message = String.format(getActivity().getString(R.string.records_deleted), mSelected.size(), mSelected.size() == 1 ? "" : "s");
                        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                                .setAction(R.string.undo, v -> {
                                    Author.saveInTx(mSelected);
                                    refresh();
                                }).show();
                        mAdapter.notifyDataSetChanged();
                        mode.finish();
                        break;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                mActionMode = null;
                mSelected.clear();
            }

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                if (checked) {
                    mSelected.add(getItem(position));
                } else {
                    mSelected.remove(getItem(position));
                }
                if (!mSelected.isEmpty()) {
                    mode.setTitle(String.format(getActivity().getString(R.string.action_mode_text), mSelected.size()));
                }
            }
        });
    }

    private Author getItem(int position) {
        return mAdapter.getItem(position);
    }


    @Override
    public void finishActionMode() {
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    public void refresh() {
        mAdapter.notifyDataSetChanged();
    }

    public String getText() {
        String str = mAdapter.toString();
        if (str.isEmpty()) {
            return str;
        }
        return getString(R.string.author_list_title) + str;
    }
}

